# Nama Proyek
jelasin proyek apa ini

## Entity-Relationship Diagram

![ERD](docs/erd.png)

*This diagram is downloaded from Visual Paradigm*

## Setting Up

## Installation

## Running Up

**Local Serving (Development Mode)**

Open `.env` and following field

```env
DEBUG_MODE=true
```

Open terminal and execute below

```
npm install
```

**Build (Deployment Mode)**

```env
DEBUG_MODE=false
```

Open terminal and execute below

```
npm run build
```

## Program Worlflow
This project using Restfull API model. You could see the documentation in [APIS.md](docs/APIS.md).

This project also provided by a Function Requirement Document which could be seen in [some-frd.docx](https://example.com/source/to/drive)

## Semantic & Standarization

**Folder Structure**

```
dir1
\_ dir1.1
   \_ file1.1.1
   \_ file1.1.2
   \_ file1.1.3
\_ file1.2
\_ file1.3
dir2
\_ dir2.1
\_ dir2.2
   \_ file2.2.1
dir3
file4
file5
```

**Semantic Naming**

Do naming as naming because naming is naming

```bash
# format
<YYYY>_<mm>_<dd>_<type>

# example
2022_02_25_human
```
Type attribute is describe whatever you wish. It could be... \
`<type>`: human, animal, plant

## Features

- Feature 1
- Feature 2
- Feature 3

## Need to check

> There is nothing to see here

## Notes

> There is nothing to see here

## Todo

> There is nothing to see here

### Suspended

> There is nothing to see here

### Next up

> There is nothing to see here

> Remove all tasks above while everything has done, wants to continue to next tasks, or would be merged to the master routes and serve in production.

## Security Issue

## References

> There is nothing to see here

## License

MIT License & Auhtor Personal License

```
You would full filled this section by MIT License and also agree to give your souls to us.
```
