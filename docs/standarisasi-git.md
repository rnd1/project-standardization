# Standarisasi

Pada bagian ini akan menstandarisasi pengelolaan dan bekerja dengan git dengan menerapkan semantic naming.

## Semantic Commit

Commits mungkin akan jarang dibaca ulang. Namun, commits ini akan membantu saat melakukan evaluasi dan ditemui error/conflict yang menyusahkan. Untuk membantu hal tersebut, sebaiknya Commit diberikan dengan cara yang semantic.

```bash
# format
[<status>](<scope>) <feature-name>: <statement>
[<status|scope>] <feature-name>: <statement>
<status|scope>: <statement>

# contoh
[hotfix] student-list: cors conflict in api requesting by auth
[patch](api) auth: add auth feature include login, register, forgot, and reset
```

`<status>`: add, patch, enable, disable, fix, bugfix, hotfix, `rev`ision, `ref`actor, remove, merge. \
`<scope>`: test, doc, conflict, `feat`ure, section

Scope di atas merupakan jenis umum. Untuk setiap framework/project dapat memiliki scope yg berbeda. Sebagai contoh: \
`<scope>` di BE: api, feat, view, util, service, repository, middleware, rules, models, dsb. \
`<scope>` di FE: api, page, section, style, asset, util, logic, dsb.

Jenis scope di atas dapat digabung menjadi mixed, feat, dan page. Standarisasi ini dapat ditetapkan kembali menyesuaikan kesepatakan proyek.

## Semantic Tagging

Tagging merupakan salah satu fitur git di mana kita dapat memilih atau kembali ke commit tertentu. Cara ini biasanya menggunakan `git checkout`. Namun melakukan checkouting perlu mengingat hash commit yang dimaksud. Untuk itu, dibanding mengingat hash, commit dapat dilabeli dengan tags sehingga perpindahan dapat cepat dan mudah.

Sebelumnya, penanda versi stabil akan menggunakan branching. Versi stabil terisolasi ini akan baik jika setiap versi dapat digunakan secara pararel, seperti python v2.x dan v3.x. Namun untuk proyek yang linear, lebih baik menggunakan tagging ini. Untuk itu, tags akan digunakan untuk versioning.

```bash
# format
v<major>.<minor>.<revisions>
<version-number>-<build-type>.<date/YYYYMMdd>

# rekomendasi penomoran
v*.0 => breaking changes/initiation
v*.1 => underdevelopment/alpha
v*.2 => MVP/beta
v*.5 => stable for use (half functionality)
v*.7 => fully functional

v*.*.# => revisions would increse if there are fixes in the same minor and had finish one lifecycle

# contoh
v0.0-alpha
v1.2-beta
v1.2-beta.20211231
v1.5-stable
v1.5-stable.20220103
v1.5-stable.20220107
v1.7-release
v1.7-stable.20220112
v1.7-nightly.20220118
v1.7.02-nightly

```

`<build-type>`: alpha, beta, unstable, experimental, nightly, stable, release, end-of-life

Standarisasi di atas mengikuti panduan Semantic Versioning dengan sedikit penyesuaian untuk mempermudah penomoran. Bagian ini akan menjelaskan secara singkat dan padat dari apa yang sudah dijelaskan di dalam SemVer. Penyesuaian sendiri dilihat dari penggunaan revisions yang jarang dan lebih mendahulukan build type. Dalam satu versi dapat memiliki 1 tipe yang sama namun dengan penanggalan yang berbeda untuk menjelaskan bahwa perubahan terjadi dalam 1 fase yang sama. Penjelasan build type sebagai berikut:

- `alpha`: saat proyek baru pertama kali dibuat dan belum memenuhi konsep MVP
- `beta`: saat proyek baru tersebut sudah dapat dikatakan stabil dan dapat dikatakan MVP. Versi ini juga disebut stagging version.
- `unstable`: tahap awal pengembangan baru. Tahap ini dapat dimulai saat versi stable/release sudah terjadi dan mulai menambahkan perubahan/fitur baru.
- `experimental`: saat pengembangan dilakukan untuk versi experimental dengan fitur begitu banyak dan tidak stabil, namun pantas digunakan.
- `nightly`: saat pengembangan dilakukan untuk hotfix dan berisi revision release.
- `stable`: saat pengembangan sudah mencapai build release.
- `release`: saat pengembangan sudah mencapai minor/major release.
- `end-of-life`: saat pengembangan sudah mencapai tahap terakhir dan memutuskan untuk mengakhiri proyek (tidak akan mendapatkan dukungan kedepannya).

Proyek ini akan dimulai dengan `alpha` di mana fitur-fitur masih sedikit dan program belum stabil/layak digunakan. Saat fitur dasar telah dibuat, proyek tersebut dapat dikatakan MVP, saat-saat inilah versi dapat dikatakan `beta`. Setelah memasuki MVP, proyek tersebut akan memasukin masa lifecycle selama pengembangan. Lifecycle ini terdiri atas `unstable-experimental-nightly-stable-release`. Sebuah proyek pasti akan mencapai masa akhir dan tidak akan didukung lagi, tahap ini akan memasuki versi `end-of-life`. Versi major yang berada dalam `v0.*` dapat dikatakan masih dikembangkan dan belum stabil. Saat versi mencapai `v1.0`, sebuah aplikasi secara keseluruhan sudah stabil dan setidaknya memiliki fitur MVP yang layak dipakai. 

Peningkatan versi minor dilakukan untuk memperlihatkan progress yang cukup signifikan. Apabila terdapat perubahan pada aplikasi di versi minor yang sama namun lifecyclenya berbeda, bisa menggunakan versi revisions dimulai dari 1. Peningkatan versi revisi hanya dilakukan apabila sudah mencapai 1 lifecycle dan ingin memulai lifecycle yang baru, sebagai contoh `v1.8-stable` dan ternyata ada perbaikan dan belum diuji tetapi ingin diversikan, bisa menjadikan `v1.8.1-unstable`. Bila kemudian sudah stabil, bisa masuk ke `v1.8.1-stable`. Jika kemudian ada fitur baru akibat penambahan fitur yang hanya dikerjakan semalam, ini berarti versi `nightly` sudah berada dalam lifecyle yang baru. Oleh karena itu, nomor revisi harus ditingkatkan menjadi `v1.8.2-nightly`. Jika minsalkan saja sudah ke versi stable, berarti masih dalam lifecyle yang sama, yaitu `v1.8.2-stable`. Kemudian ada penambahan atau perbaikan fitur namun sudah dapat dipastikan stable, cukup gunakan penanggalan menjadi `v1.8.2-stable.20220214`. Versi ini jika dilanjutkan akan menjadi `v1.8.2`, `v1.8.3`, `v1.8.9`, `v1.8.10`, `v1.8.11`, dan seterusnya. 

> CATATAN: Sebaiknya cukup berikan tag saat aplikasi sudah stabil sehingga penomoran tidak memerlukan penulisan revision. Jika masih dalam tahap uji coba dan pengguna tidak terlalu memerlukan kembali ke versi ini, sebaiknya tidak diperlukan tagging. Jika memang benar-benar diperlukan, berikan nomor tagging dan perhatikan bagaimana cara menulis revisi, build, dan tanggal dengan benar.

> CATATAN: penulisan versi minor maupun major saat sudah mencapai 9 dapat ditingkatkan menjadi 10, kemudian 11, 12, 13, dan seterusnya. Sebagai contoh `v1.8.2`, `v1.8.3`, `v1.8.9`, `v1.8.10`, `v1.8.11`, `v1.8.12`, `v1.8.23`, `v1.8.99`, `v1.8.100`.


> CATATAN: rekomendasi penomoran sebelumnya digunakan untuk breaking changes. Sebagai contoh, saat aplikasi sudah melewati versi `v0.*` dan menjadi `v1.0`, maka penggunaan minor dari 1-7 akan menjadi penyimbolan. Tidak ada panduan khusus lainnya, gunakan dengan bijak.

Untuk versi major ditingkatkan saat proyek dimulai kembali dengan spesifikasi baru, akan ada perubahan yang cukup besar (breaking changes), atau saat yearly sprint. Tingkatkan nilai hanya memang dibutuhkan saja. Bila hanya perubahan kecil atau hanya penambahan beberapa fitur tanpa mengubah sebagian sistem, gunakan penomoran revisions atau penanggalan.

> PERHATIKAN: pembuatan tags ini hanya dilakukan jika versi saat itu sudah cukup stabil dan memenuhi konsep MVP, ini berlaku untuk tahap stable dan release. Penggunaan versi lain di bawah stable hanya dilakukan jika versi saat itu sudah cukup teruji, saat monthly release, atau memang benar-benar dibutuhkan.

## Semantic Branching

Branching digunakan untuk mengisolasi suatu fitur/perbaikan dari sistem utama. Dengan mengisolasi ini, fitur yang sedang dikembangkan tidak akan mengganggu pengembangan fitur lainnya dan tidak akan merusak program utama (yang sudah stabil). Metode ini juga digunakan untuk mempermudah pelacakan dan perbaikan fitur spesifik tanpa terganggu kode fitur lainnya.

Dalam membuat suatu branch juga memiliki format semantic yang dapat mempermudah pencarian. Format semanticnya seperti berikut:

```bash
# format
<type>/<version>/<scrum-title>/<scrum-number>/<date>

# contoh
main                      # <type>
develop                   # <type>
test                      # <type>
test/2021-12-30           # <type>/<date>
stable/2022-01-02         # <type>/<date>
release/1.0.0             # <type>/<number>
experimental/test-graphql # <type>/<title>
feature/auth/BIMAY-12     # <type>/<scrum-title>/<scrum-number>
```

Format type kebanyakan mengikuti semantic committing. \
`<type>`: main, develop, test, docs, release, experimental, hotfix, bugfix, feature, page, section, component

Tipe stable dan release hanya akan digunakan bila pengembangan proyek dilakukan secara pararel (setiap versi memang memiliki tujuan yang berbeda dan masih terus dikembankan). Sebagai contoh Python yang masih mengembangkan versi 2.x dan 3.x secara bersamaan. Jika pengembangan dilakukan secara linear, lebih baik menggunakan tags.

Secara umum, `develop` merupakan fork dari main. Fungsi develop adalah sebagai penyatuan semua branch pengembangan sebelum dilakukan merging ke main. Hal ini dilakukan untuk menghindari masalah di build asli. Dalam praktiknya, setelah semua branch `feature`, `page`, dan `component` di-merge ke `develop`, developer/QA akan membuild dan mengetes semua fitur dari satu project utuh di sini. Pengujian dapat dilakukan di server dalam domain yang berbeda, seperti stagging, atau di server development. Branch yang digunakan/dideploy di domain dan server tersebut ialah `develop`. Saat sudah dapat dipastikan bahwa buildnya berhasil, developer akan memerge ke `main`. Akhirnya, developer akan mendeploy branch `main` ke server production.

## Semantic Merging

**Saat Merge Request**

Saat suatu fitur/perbaikan sudah selesai dikembangkan, branch tersebut akan dilakukan merging ke branch utama - master. Sebelum melakukan merging ini, perlu dilakukan merge request terlebih dahulu. Hal ini dilakukan untuk mendapatkan review terhadap kode terkait untuk memastikan ulang fiturnya dapat digabungkan. Untuk mempercepat review, sebaiknya menggunakan penamaan dan metode yang sesuai. Penulisannya dapat sebagai berikut:

1. Gunakan judul merge-request seperti berikut

```bash
# format
[<status>](<scrum-number>) <branch-name>: <short description>

# contoh
[hotfix] feature/change-password/TRAP-12: refactoring code
[hotfix](TRAP-12) feature/change-password: refactoring code
```

Format status mirip dengan Semantic Committing, bedanya tidak menggunakan `[merge]`.

2. Berikan deskripsi yang jelas

Jelaskan apa yang dilakukan pada merge request tersebut, apa saja perubahaannya, kendala yang ditemukan, hingga konsiderasi selanjutnya bila mengira akan ada masalah di masa yang akan datang. Format penulisannya bisa seperti berikut:

```markdown
Describe the problem has been discovered. Say the way how you are doing in this merge.

**Runtime Information**

Describe what OS is used to run this program.
Describe what version of this program is used.
Show the program log or error log which could be introgate.

**Changelog**

1. Fix models to support unincremented primary key
2. Second code changes or problem has been solved.
3. Third feature has been changes or problem has been solved.

**Future problem**

1. There would be conflict in any Response type
state your recommendation or considerationa about this problem.

2. Second problem has not been solved.
state your recommendation or considerationa about this problem.

3. Third problem has not been solved.
state your recommendation or considerationa about this problem.

**Conclusion**

State a conclusion or recommendation along this merge.
```

3. Jangan centang `delete this branch after merging`
4. Kemudian buat merge request
5. Assign dirimu ke merge request ini
6. Tetapkan reviewer dari salah satu senior developer

**Saat merge conflict atau setelah merging pada Merge Request**

Saat menyetujui Merge Request, maintainer biasanya akan menemui Merge Conflict. Conflict ini akan diperbaiki terlebih dahulu sehingga akan menciptakan commit terbaru. Git maupun Git RepHost akan membuat message-nya secara otomatis, namun lebih baik mengikuti format berikut:

```
# format
[merge] <branch-name>: <description>
See merge request !<hash-number>

# contoh
[merge] hotfix/change-password-return-403: return 403 HTTP Response when password not match
See merge request !29
```

Sebagai tambahan untuk maintainer, setelah merge request selesai dilakukan, branch feature, page, dan section sebaiknya tidak dihapus. Namun, untuk branch seperti hotfix, bugfix, dan fix dapat dihapus setelah merge selesai dan program sudah dapat dipastikan bekerja sempurna.

**Saat merge conflict dengan Pull**

Ada kalanya merge conflict terjadi di working branch yang sama. Sebagai contoh, dua orang developer bekerja di branch master dan mengubah beberapa file yang sama. Salah satunya melakukan push terlebih dahulu. Akhirnya, saat yang lainnya melakukan push akan diminta melakukan pull terlebih dahulu. Keadaan ini akan memunculkan conflict. Untuk itu, developer tadi perlu memilah kode mana yang akan di push ke master dan menjadi kode utamanya. Setelah itu, lalu di commit dan di push.

Saat commit inilah, pesan yang diberikan dapat berbeda. Untuk itu formattingnya dapat seperti berikut:

```bash
# format
[merge] <feature-name>: <statement>

# contoh
[merge] auth-view: using bootstrap instead tailwind
```

Penamaan ini sebenarnya mirip dengan yang dibuat pada Semantic Commit. Jadi bisa mengikuti format yang sama.

## Semantic Versioning

Terdapat beberapa model yang digunakan untuk memberikan versi pada aplikasi. Umumnya, model yang digunakan ialah format SemVer, Build-Revision, dan Release Type. Penamaan versi pun dapat beragam tergantung organisasi yang menaunginya. Untuk itu, perlu cara termudah dalam penamaan tanpa membuat kebingungan.

Untuk ringkasan, berikut format sederhananya:

```
# format
v<major>.<minor>.<patch>                # Semantic Versioning
v<major>.<minor>.<build>.<revision>     # Build Revision Versioning
v<major>.<minor> <build-type> (<date/YYYY-MM-dd>)  # Release Versioning
v<major>.<minor>-<build-type>.<date/YYYYMMdd>

# contoh
v1.05.0072
v1.5.072.00012
v1.5 unstable (2022-01-02)
```

Seperti pada Semantic Tagging, standarisasi ini akan menggunakan Release Versioning untuk mempermudah pemahaman. Kelebihan dari Release Versioning ini adalah bisa mengetahui secara cepat tipe suatu proyek, sayangnya ini hanya berlaku untuk proyek berskala kecil. Untuk `<date>` sendiri dapat diambil pada hari itu tags dibuat.

## Catatan
- Standarisasi di atas ditulis berdasarkan pengalaman dan riset. Untuk referensi sendiri dapat dilihat pada sesi selanjutnya.
- Pada bagian tambahan untuk maintaner setelah melakukan merge request sepenuhnya referensi pribadi dan tidak di dukung dengan panduan apapun. Sebaiknya diriset lebih dalam lagi mengenai "working with git branch in developer team". Saat poin ini ditulis, belum ditemukan artikel yang cukup semantic.

## Referensi
- https://en.wikipedia.org/wiki/Software_release_life_cycle
- https://stackoverflow.com/a/46967235
- https://en.wikipedia.org/wiki/Software_versioning
- https://git-scm.com/book/en/v2/Git-Basics-Tagging
- https://semver.org/
- https://docs.gitlab.com/ee/user/project/releases/index.html#create-a-release
- https://docs.gitlab.com/ee/user/project/releases/index.html#tag-name
- https://docs.gitlab.com/ee/policy/maintenance.html#versioning
